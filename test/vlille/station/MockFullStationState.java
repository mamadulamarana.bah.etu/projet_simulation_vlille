package vlille.station;

import java.util.List;
import java.util.Random;

import vlille.station.util.EmptyStationException;
import vlille.station.util.EmptyStationState;
import vlille.station.util.StableStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class MockFullStationState extends StationState{

	public MockFullStationState(Station<Vehicle> station) {
		super(station);
	}
	
	@Override
	public void empty() {
		this.station.setState(new MockEmptyStationState(this.station));
	}

	@Override
	public void full() {
		//
	}
	
	@Override
	public void stable() {
		this.station.notifyCenter();
		this.station.setState(new MockStableStationState(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = 0; // le mock est fait pour tester l'aléatoire ici
		int nbTransferedVehicle = 4; // et là
		System.out.println("ici");
		while(nbTransferedVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station))&& (choosenStation.getFreeSlot()> 0) ) {
					if (nbTransferedVehicle == choosenStation.getFreeSlot()) {
						nbTransferedVehicle = this.station.transferAllAvailableRentedTo(choosenStation);
					}
					else {
						nbTransferedVehicle = this.station.transferNbVehicleToanotherStation(choosenStation, nbTransferedVehicle);
					}
				}
				index = 0;
			}catch(StationFullException e) {
				index = 0;
			}catch(EmptyStationException e) {
				System.out.println(this.station);
			}
		}
		if(this.station.isEmpty()) {
			this.empty();
		}
		else {
			this.stable();
		}
	}

	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is full";
	}
	
}
