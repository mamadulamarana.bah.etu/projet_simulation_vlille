package vlille.station;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vlille.center.Center;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.*;

public abstract class StationTest{

	protected Station<Vehicle> station;
	protected abstract Station<Vehicle> createStation();
	
	@BeforeEach
	public void init() {
		this.station = this.createStation();
	}
	
	@Test
	public void testCapacityofStation() {
		int capacity = 10;
		Station<Vehicle> station = new MockBikeStation(2, capacity);
		assertTrue(station.getCapacity() == capacity);
	}
}
