package vlille.station;

import static org.junit.jupiter.api.Assertions.*;


import java.util.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import vlille.center.Center;
import vlille.center.MockStableStationState2;
import vlille.station.util.EmptyStationException;
import vlille.station.util.EmptyStationState;
import vlille.station.util.FullStationState;
import vlille.station.util.StableStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.UserCannotRentException;
import vlille.user.User;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.Bike;
import vlille.vehicle.bike.BikeIsNotAvailableException;
import vlille.vehicle.util.Basket;
import vlille.vehicle.util.OutOfServiceState;
import vlille.vehicle.util.VehicleDecorator;
import vlille.vehicle.util.VehicleWithRack;
import vlille.vehicle.bike.ClassicBike;
import vlille.vehicle.bike.ElectricBike;

public class BikeStationTest extends StationTest {

	int id = 1;
	
	protected Station<Vehicle> createStation() {
		Station<Vehicle> station = new BikeStation(id);
		station.setCenter(Center.getInstance());
		return station;
	}
	
	@AfterEach
	public void tearDown() {
		this.station.getCenter().reinitializeCenter(); /**supprime toutes les stations pour eviter les effets de bord*/
	}
		
	@Test
	public void testIdofAStation() {
		assertEquals(this.station.getId(), id);
	}

	@Test
	public void testIfARentedBikeAreAdded() throws StationFullException {
		try {
			assertTrue(this.station.getAvailable().size() == 0);
			ElectricBike electricBike = new ElectricBike(1);
			ClassicBike classicBike = new ClassicBike(1);
			Basket basket = new Basket(classicBike);
			basket.getType();
			this.station.add(electricBike);
			this.station.add(classicBike);
			assertEquals(this.station.getAvailable().size(), 2);
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testToAddRentableBikeIfStationIsFull() throws StationFullException {
		assertTrue(this.station.getAvailable().size() == 0);
		assertThrows(StationFullException.class, () -> {
			for (int i=0; i<this.station.getCapacity()+1; i++) {
				ClassicBike bike = new ClassicBike(1);
				bike.setId(i);
				this.station.add(bike);
			}
		});
	}
		
	@Test
	public void testAddAListOfRentableBikeToAStation() throws StationFullException {
		try {
			assertTrue(this.station.getAvailable().size() == 0);
			ElectricBike electricBike = new ElectricBike(1);
			ClassicBike classicBike = new ClassicBike(1);
			List<Bike> l = new ArrayList<>();
			l.add(classicBike);
			l.add(classicBike);
			l.add(electricBike);
			this.station.addList(l);
			assertEquals(this.station.getAvailable().size(), 2);		
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testTransferAllRentableBikeToAnotherStation() throws StationFullException, EmptyStationException {
		try {
			ClassicBike classicBike = new ClassicBike(1);
			ElectricBike electricBike = new ElectricBike(1);
			VehicleDecorator bikeWithRack = new VehicleWithRack(classicBike);
			VehicleDecorator bikeWithBasket = new Basket(electricBike);
			List<Vehicle> l = new ArrayList<>();
			l.add(classicBike);
			l.add(bikeWithBasket);
			l.add(electricBike);
			l.add(bikeWithRack);
			this.station.addList(l);
			assertTrue(this.station.getAvailable().size() == 4);
			assertEquals(this.station.getOccupiedSlot(), 4);
			Station<Vehicle> station1 = new BikeStation(2);
			station1.setCenter(Center.getInstance());
			int nb = this.station.transferAllAvailableRentedTo(station1);
			assertEquals(this.station.getOccupiedSlot(), 0);
			assertEquals(station1.getOccupiedSlot(), 4);
			assertEquals(nb, 0);
		}catch(StationFullException | EmptyStationException e) {
			fail();
		}
	}
	
	/**
	 * add rentable object on station, if duplicates object
	 * transfer to another station
	 * @throws StationFullException 
	 */
	@Test
	public void testToAddRentableBikeAndTransferDuplicates() throws StationFullException {
		try {
			ClassicBike classicBike = new ClassicBike(1);
			ElectricBike electricBike = new ElectricBike(1);
			VehicleDecorator bikeWithRack = new VehicleWithRack(classicBike);
			VehicleDecorator bikeWithBasket = new Basket(electricBike);
			List<Vehicle> l = new ArrayList<>();
			l.add(classicBike); l.add(bikeWithRack);
			l.add(electricBike); l.add(bikeWithBasket);
			this.station.addList(l);
			assertEquals(this.station.getOccupiedSlot(), 4);
			l.add(classicBike); l.add(electricBike); l.add(new ElectricBike(6));
			Station<Vehicle> station1 = new BikeStation(2);
			station1.setCenter(Center.getInstance());
			this.station.addGoodsAndTransferDuplicate(l, station1);
			assertEquals(this.station.getOccupiedSlot(), 4);
			assertEquals(classicBike, electricBike);
			assertEquals(station1.getAvailable().size(), 3);
		}catch(StationFullException e) {
			fail();
		}
	}
		
	@Test
	public void testWeCanNotTransferAVehicleWhenIsOutOfService() throws StationFullException, BikeIsNotAvailableException {
		try {
			ClassicBike classicBike = new ClassicBike(1);
			this.station.add(classicBike);
			Station<Vehicle> station = new BikeStation(2);
			classicBike.outOfService(); 
			this.station.transferAllAvailableRentedTo(station);
			assertEquals(this.station.getOccupiedSlot(), 1);
			assertEquals(station.getOccupiedSlot(), 0);
		}catch(StationFullException|EmptyStationException e) {
			fail();
		}
	}

	@Test
	public void testRentToUserWhenStationIsEmpty() throws EmptyStationException{
		assertEquals(this.station.getOccupiedSlot(), 0);
		assertThrows(EmptyStationException.class, ()-> {
			this.station.rentTo(null, 1);
		});
	}
		
	@Test
	public void testRentToUser() throws EmptyStationException, StationFullException, BikeIsNotAvailableException{
		try {
			User user = new User("Mamad", 18);
			ClassicBike classicBike = new ClassicBike(1);
			ElectricBike electricBike = new ElectricBike(1);
			this.station.add(classicBike); this.station.add(electricBike);
			int actual = this.station.getOccupiedSlot();
			Vehicle rented = this.station.rentTo(user, 0);
			assertTrue(this.station.getOccupiedSlot() < actual);
		}catch(EmptyStationException | UserCannotRentException | BikeIsNotAvailableException e) {
			fail();
		}
	}

	@Test
	public void testRentToUserWhenVehicleIsOutOfService() throws StationFullException, BikeIsNotAvailableException {
		try {
			User user = new User("Mamad", 18);
			ClassicBike classicBike = new ClassicBike(1);
			this.station.add(classicBike);
			classicBike.outOfService();
			assertThrows(BikeIsNotAvailableException.class, () -> this.station.rentTo(user, 0));
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testStationIsOnEmptyStateInitially() throws StationFullException {
		assertEquals(this.station.getOccupiedSlot(), 0);
		assertTrue(this.station.getState() instanceof EmptyStationState);
	}
	
	@Test
	public void testStationIsStableWhenHeHasVehicleButNotFull() throws StationFullException {
		try {
			assertTrue(this.station.getState() instanceof EmptyStationState);
			this.station.add(new ClassicBike(1));
			assertTrue(this.station.getState() instanceof StableStationState);
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testStationIsFullWhenheHasreachedHisCapacity() {
		try {
			Station<Vehicle> st = new MockBikeStation(1, 1);
			assertTrue(st.getState() instanceof MockEmptyStationState);
			st.add(new ClassicBike(1));
			assert(st.getState() instanceof MockFullStationState);
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testStationIsStableAgainAfterHeIsFull() throws StationFullException, UserCannotRentException, EmptyStationException, BikeIsNotAvailableException {
		Station<Vehicle> st = new MockBikeStation(1, 2); // station with 2 capacity.
		st.setCenter(Center.getInstance());
		st.add(new ClassicBike(1)); st.add(new ElectricBike(2));
		assertTrue(st.getState() instanceof MockFullStationState);
		st.rentTo(new User("scoth", 19), 0);
		assertTrue(st.getState() instanceof MockStableStationState);
	}
	
	@Test
	public void testStationIsEmptyAfterHeIsFull() throws StationFullException, EmptyStationException {
		Station<Vehicle> st = new MockBikeStation(1, 2);
		st.setCenter(Center.getInstance());
		st.add(new ClassicBike(1)); st.add(new ElectricBike(2));
		assertTrue(st.getState() instanceof MockFullStationState);
		Station<Vehicle> st1 = new BikeStation(3);
		st1.setCenter(Center.getInstance());
		st.transferAllAvailableRentedTo(st1);
		assertTrue(st.getState() instanceof MockEmptyStationState);
	}
	
	@Test
	public void testStationIsEmptyOrFullAfterHeIsStable() throws StationFullException, UserCannotRentException, EmptyStationException, BikeIsNotAvailableException {
		Station<Vehicle> st = new MockBikeStation(1, 3);
		st.setCenter(Center.getInstance());
		st.add(new ClassicBike(1)); st.add(new ElectricBike(2));
		assertTrue(st.getState() instanceof MockStableStationState);
		st.rentTo(new User("scoth", 19), 0);
		assertTrue(st.getState() instanceof MockStableStationState);
		st.rentTo(new User("mamad", 18), 0);
		assertTrue(st.getState() instanceof MockEmptyStationState);
		st.add(new ClassicBike(1)); st.add(new ElectricBike(2));
		assertTrue(st.getState() instanceof MockStableStationState);
		st.add(new ElectricBike(4));
		assertTrue(st.getState() instanceof MockFullStationState);
	}
	
	@Test
	public void testTransferNbVehicleInAnotherStation() throws StationFullException, BikeIsNotAvailableException {
		for (int i=0; i<4; i++) {
			this.station.add(new ClassicBike(i));
		}
		Station<Vehicle> st = new MockBikeStation(2, 4);
		st.setCenter(Center.getInstance());
		assertEquals(st.getOccupiedSlot(), 0);
		int nb = this.station.transferNbVehicleToanotherStation(st, 4);
		assertEquals(st.getOccupiedSlot(), 4);
		assertEquals(nb, 0);
	}
	
	/**
	 * there is a vehicle which cannot transfer (it is outOfService)
	 * @throws StationFullException 
	 * @throws BikeIsNotAvailableException 
	 */
	@Test
	public void testTransferNbVehicleInAnotherStation1() throws StationFullException, BikeIsNotAvailableException {
		for (int i=0; i<4; i++) {
			this.station.add(new ClassicBike(i));
		}
		this.station.getAvailable().get(0).outOfService();
		Station<Vehicle> st = new MockBikeStation(2, 4);
		st.setCenter(Center.getInstance());
		assertEquals(st.getOccupiedSlot(), 0);
		int nb = this.station.transferNbVehicleToanotherStation(st, 4);
		assertEquals(st.getOccupiedSlot(), 3);
		assertEquals(nb, 1);
	}
	
	@Test
	public void testUpdateStationWhenItIsEmpty() throws StationFullException {
		try {
			int capacity = 4;
			Station<Vehicle> st = new MockBikeStation(3, capacity);
			st.setCenter(Center.getInstance());
			for (int i=0; i<capacity; i++) {
				this.station.add(new ClassicBike(i));
			}
			assertEquals(this.station.getOccupiedSlot(), 4);
			assertEquals(st.getOccupiedSlot(), 0);
			st.update(st.getCenter().getStations()); // update station (st) which is empty
			assertEquals(st.getOccupiedSlot(), 3);
			assertEquals(this.station.getOccupiedSlot(), 1);
		}catch(StationFullException e) {
			fail();
		}
	}
	
	@Test
	public void testupdateStationWhenItIsFull() throws StationFullException{
		try {
			int capacity = 4;
			Station<Vehicle> st = new MockBikeStation(4, capacity);
			st.setCenter(Center.getInstance());
			for (int i=0; i<capacity; i++) {
				st.add(new ClassicBike(i));
			}
			//st.setState(new MockFullStationState(st));
			assertEquals(st.getOccupiedSlot(), 4);
			st.update(st.getCenter().getStations()); // update st which is full
			assertEquals(st.getOccupiedSlot(), 0);
			assertEquals(this.station.getOccupiedSlot(), 4);
		}catch(StationFullException e) {
			fail();
		}
	}
		
}
