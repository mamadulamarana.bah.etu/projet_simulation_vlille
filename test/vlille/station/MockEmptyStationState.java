package vlille.station;

import java.util.List;

import java.util.Random;

import vlille.station.util.EmptyStationException;
import vlille.station.util.StableStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;

public class MockEmptyStationState extends StationState{

	public MockEmptyStationState(Station<Vehicle> station) {
		super(station);
	}

	@Override
	public void empty() {
		//
	}

	@Override
	public void full() {
		this.station.setState(new MockFullStationState(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void stable() {
		this.station.notifyCenter();
		this.station.setState(new MockStableStationState(this.station));
		System.out.println(this.station);
	}
	
	/**le mock est fait pour nbTakenVehicle et index qui est de base aléatoire*/
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = 0;
		int nbTakenVehicle = 3;
		while(nbTakenVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station)) && (choosenStation.getOccupiedSlot()>0) ) {
					if (nbTakenVehicle == choosenStation.getOccupiedSlot()) {
						nbTakenVehicle = choosenStation.transferAllAvailableRentedTo(this.station);
					}
					else {
						nbTakenVehicle = choosenStation.transferNbVehicleToanotherStation(this.station, nbTakenVehicle);
					}
				}
				index = 0;
			}
			catch(EmptyStationException e) {
				index = 0;
			}catch(StationFullException e) {
				System.out.println(this.station);
			}
		}
		this.stable();
	}
	
	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is empty";
	}
}
