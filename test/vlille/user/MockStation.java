package vlille.user;

import vlille.center.Observer;
import vlille.station.Station;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;

public class MockStation extends Station<Vehicle>{

	public MockStation(int id) {
		super(id);
		this.capacity = 0;
	}

	@Override
	public void setState(StationState state) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyCenter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setCenter(Observer observer) {
		// TODO Auto-generated method stub
		
	}

}
