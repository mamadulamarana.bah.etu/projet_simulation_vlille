package vlille.user;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vlille.center.Center;
import vlille.station.*;
import vlille.station.util.EmptyStationException;
import vlille.station.util.StationFullException;
import vlille.station.util.UserCannotRentException;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.Bike;
import vlille.vehicle.bike.BikeIsNotAvailableException;
import vlille.vehicle.bike.ClassicBike;
import vlille.vehicle.bike.ElectricBike;

public class UserTest {
	
	protected User user;
	protected BikeStation station;

	@BeforeEach
	public void init() {
		this.user = new User("Mamad", 18);
		this.station = new BikeStation(1); /** a user rented from a station*/
		this.station.setCenter(Center.getInstance()); /** always assign a center to a station before using it */
	}
		
	@AfterEach
	public void tearDown() {
		this.station.getCenter().reinitializeCenter();
	}
	
	
	@Test
	public void testUserRentSomething() throws StationFullException, UserCannotRentException,
											EmptyStationException, BikeIsNotAvailableException {
		try {
			assertEquals(this.user.AreRentingSomething(), false);
			assertEquals(this.user.getNumberRentedObject(), 0);
			ClassicBike classicBike = new ClassicBike(1);
			ElectricBike electricBike = new ElectricBike(1);
			this.station.add(classicBike); this.station.add(electricBike);
			this.user.rentSomething(this.station);
			assertEquals(this.user.AreRentingSomething(), true);
			assertEquals(this.user.getNumberRentedObject(), 1);
		}catch(UserCannotRentException e) {
			fail();
		}
	}
	
	@Test
	public void testRentToUserWhenHeIsCannotRentSomething() throws StationFullException, UserCannotRentException{
		User user = new User("Mamad", 17);
		ElectricBike electricBike = new ElectricBike(1);
		this.station.add(electricBike);
		assertThrows(UserCannotRentException.class, () -> {
			user.rentSomething(this.station);
		});
	}
			
	@Test
	public void testUserWantRentWhenVehicleIsStolen() throws StationFullException, BikeIsNotAvailableException {
		ClassicBike classicBike = new ClassicBike(1);
		this.station.add(classicBike);
		classicBike.stole();
		assertThrows(BikeIsNotAvailableException.class, ()-> user.rentSomething(station));
	}
	
	@Test
	public void testUserWantRentWhenVehicleIsOutOfService() throws StationFullException, BikeIsNotAvailableException {
		ClassicBike classicBike = new ClassicBike(1);
		this.station.add(classicBike);
		classicBike.outOfService();
		assertThrows(BikeIsNotAvailableException.class, ()-> user.rentSomething(station));
	}
	
	@Test
	public void testUserWantRentWhenVehicleIsAlreadyRented() throws StationFullException, BikeIsNotAvailableException {
		ClassicBike classicBike = new ClassicBike(1);
		this.station.add(classicBike);
		classicBike.rent();
		assertThrows(BikeIsNotAvailableException.class, ()-> user.rentSomething(station));
	}
	
	@Test
	public void testUserReturnRentedObjectToAStation() throws StationFullException, UserCannotRentException,
															EmptyStationException, BikeIsNotAvailableException {
		assertEquals(this.user.getNumberRentedObject(), 0);
		assertEquals(this.user.AreRentingSomething(), false);
		ClassicBike classicBike = new ClassicBike(1);
		this.station.add(classicBike);
		this.user.rentSomething(this.station);
		assertEquals(this.user.AreRentingSomething(), true);
		assertEquals(this.station.getOccupiedSlot(), 0);
		this.user.returnRentedVehicle( this.station, this.user.getRentedVehicle());
		assertEquals(this.station.getOccupiedSlot(), 1);
		assertEquals(this.user.getNumberRentedObject(), 0);
	}
	
	@Test
	public void testUserReturnRentedObjectToAStationButItIsFull() throws StationFullException {
		try {
			ClassicBike classicBike = new ClassicBike(1);
			this.station.add(classicBike);
			this.user.rentSomething(this.station);
			assertEquals(this.station.getOccupiedSlot(), 0);
			Station<Vehicle> station = new MockBikeStation(2, 1); /** on utilise parce que la capacité est aléatoire */
			station.add(classicBike);
			assertThrows(StationFullException.class, ()->
			this.user.returnRentedVehicle(station, this.user.getRentedVehicle()));
		}catch(UserCannotRentException|BikeIsNotAvailableException|EmptyStationException e) {
			fail();
		}
	}


}
