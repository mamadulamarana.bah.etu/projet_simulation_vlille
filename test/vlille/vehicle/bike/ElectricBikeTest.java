package vlille.vehicle.bike;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;

import vlille.vehicle.Vehicle;
import vlille.vehicle.util.Basket;
import vlille.vehicle.util.VehicleDecorator;
import vlille.vehicle.util.VehicleWithRack;

public class ElectricBikeTest extends VehicleTest {

	protected Vehicle createBike() {
		return new ElectricBike(1);
	}
	
	@Test
	public void testIsElectric() {
		assertTrue(this.vehicle instanceof ElectricBike);
	}
	
	@Test
	public void testClassicBikeDecoratedWithBasket() {
		VehicleDecorator bikeWithBasket = new Basket(this.vehicle);
		assertTrue(bikeWithBasket instanceof Basket);
		assertEquals(bikeWithBasket.getType(), this.vehicle.getType()+" with basket");
	}

	@Test
	public void testElectricBikeWithRack() {
		assertEquals(this.vehicle.getType(), "Electric bike");
		VehicleDecorator bikeWithBasket = new Basket(this.vehicle);
		assertEquals(bikeWithBasket.getType(), this.vehicle.getType()+" with basket");
	}
	
	@Test
	public void testBikeIsDecoratedWithRackAndBasket() {
		assertEquals(this.vehicle.getType(), "Electric bike");
		VehicleDecorator bikeWithBasket = new Basket(this.vehicle);
		VehicleDecorator bikeWithBasketAndRack = new VehicleWithRack(bikeWithBasket);
		assertEquals(bikeWithBasketAndRack.getType(), this.vehicle.getType()+" with basket with rack");
	}



}
