package vlille.vehicle.bike;

import static org.junit.jupiter.api.Assertions.*;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vlille.vehicle.*;
import vlille.vehicle.util.AvailableState;
import vlille.vehicle.util.OutOfServiceState;
import vlille.vehicle.util.RentedState;
import vlille.vehicle.util.StolenState;

public abstract class VehicleTest {

	protected Vehicle vehicle;
	
	protected abstract Vehicle createBike();

	@BeforeEach
	public void init() {
		this.vehicle = this.createBike();
	}
		
	@Test
	public void testBikeIsOnAvailableStateAtFirst() {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
	}
	
	@Test
	public void testIfChangeTheStateOfaBikeWork() throws BikeIsNotAvailableException {
		this.vehicle.rent();
		assertTrue(this.vehicle.getState() instanceof RentedState);
		this.vehicle.available();
		this.vehicle.outOfService();
		assertTrue(this.vehicle.getState() instanceof OutOfServiceState);
		this.vehicle.outOfService();
		assertTrue(this.vehicle.getState() instanceof OutOfServiceState);
		this.vehicle.available();
		this.vehicle.stole();
		assertTrue(this.vehicle.getState() instanceof StolenState);
		assertThrows(BikeIsNotAvailableException.class, ()-> this.vehicle.available());
		assertTrue(this.vehicle.getState() instanceof StolenState);
	}
	
	@Test
	public void testBikeCanBeRentedWhenItIsAvailable() throws BikeIsNotAvailableException {
		try {
			assertTrue(this.vehicle.getState() instanceof AvailableState);
			this.vehicle.rent();
			assertTrue(this.vehicle.getState() instanceof RentedState);
		}catch(BikeIsNotAvailableException e) {
			fail();
		}
	}
	
	@Test
	public void testBikeCanNotBeRentedWhenItIsOutOfService() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
		this.vehicle.outOfService();
		assertThrows(BikeIsNotAvailableException.class, () -> {
			this.vehicle.rent();
		});
	}
	
	@Test
	public void testBikeCannotBeRentedWhenItIsStolen() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
		this.vehicle.stole();
		assertThrows(BikeIsNotAvailableException.class, ()-> {
			this.vehicle.rent();
		});
	}
			
	@Test
	public void testToUseVehicleWhenNbOfUseIsReached() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getNbOfUse() == 10);
		for (int i=0; i<11; i++) {
			this.vehicle.usedOneTime();
		}
		assertEquals(this.vehicle.getNbOfUse(), 0);
		assertTrue(this.vehicle.getState() instanceof AvailableState);
	}
	
	@Test
	public void testTransferVehicle() throws BikeIsNotAvailableException{
		try {
			this.vehicle.transfer();
		}catch(BikeIsNotAvailableException e) {
			fail();
		}
	}
	
	@Test
	public void testToTransferVehicleWhenItIsOutOfService() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
		this.vehicle.outOfService();
		assertThrows(BikeIsNotAvailableException.class, ()->this.vehicle.transfer());
	}
	
	@Test
	public void testToTransferVehicleWhenItIsRented() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
		this.vehicle.rent();
		assertThrows(BikeIsNotAvailableException.class, ()->this.vehicle.transfer());
	}
	
	@Test
	public void testToTransferVehicleWhenItIsStolen() throws BikeIsNotAvailableException {
		assertTrue(this.vehicle.getState() instanceof AvailableState);
		this.vehicle.stole();
		assertThrows(BikeIsNotAvailableException.class, ()->this.vehicle.transfer());
	}
		
}
