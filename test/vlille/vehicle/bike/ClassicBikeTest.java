package vlille.vehicle.bike;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import vlille.vehicle.*;
import vlille.vehicle.util.Basket;
import vlille.vehicle.util.VehicleWithRack;
import vlille.vehicle.util.VehicleDecorator;


public class ClassicBikeTest extends VehicleTest {

	protected Vehicle createBike() {
		return new ClassicBike(1);
	}
	
	@Test
	public void testIsClassic() {
		assertTrue(this.vehicle instanceof ClassicBike);
	}
	
	@Test
	public void testClassicBikeDecoratedWithBasket() {
		assertEquals(this.vehicle.getType(), "Classic bike");
		VehicleDecorator bikeWithBasket = new Basket(this.vehicle);
		assertEquals(bikeWithBasket.getType(), this.vehicle.getType()+" with basket");
	}
	
	@Test
	public void testClassicBikeDecoratedWithRack() {
		assertEquals(this.vehicle.getType(), "Classic bike");
		VehicleDecorator bikeWithBasket = new VehicleWithRack(this.vehicle);
		assertEquals(bikeWithBasket.getType(), this.vehicle.getType()+" with rack");
	}
		
	@Test
	public void testBikeIsDecoratedWithRackAndBasket() {
		assertEquals(this.vehicle.getType(), "Classic bike");
		VehicleDecorator bikeWithBasket = new Basket(this.vehicle);
		VehicleDecorator bikeWithBasketAndRack = new VehicleWithRack(bikeWithBasket);
		assertEquals(bikeWithBasketAndRack.getType(), this.vehicle.getType()+" with basket with rack");
	}

}
