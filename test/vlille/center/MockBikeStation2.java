package vlille.center;

import vlille.center.Center;
import vlille.center.Observer;
import vlille.station.Station;
import vlille.station.util.EmptyStationState;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.Bike;

public class MockBikeStation2 extends Station<Vehicle> {

	public MockBikeStation2(int id, int mockCapacity) {
		super(id);
		this.capacity = mockCapacity;
		this.state = new MockEmptyStationState2(this);
	}

	@Override
	public void setState(StationState newState) {
		this.state = newState;
	}
	
	@Override
	public void notifyCenter() {
		this.observer.addObjectToBeUpdate(this);
	}
	
	@Override
	public void setCenter(Observer observer) {
		this.observer = observer;
		this.observer.addStation(this);	
		this.notifyCenter();
	}

	/**
	 * 
	 */
	public String toString() {
		return "Bike "+this.getState();
	}

}
