package vlille.center;

import java.util.List;

import vlille.station.Station;
import vlille.station.util.EmptyStationState;
import vlille.station.util.FullStationState;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;

public class MockStableStationState2 extends StationState{

	public MockStableStationState2(Station<Vehicle> station) {
		super(station);
	}
	
	@Override
	public void empty() {
		this.station.setState(new MockEmptyStationState2(this.station));
		this.station.notifyCenter();
		System.out.println(this.station);
	}

	@Override
	public void full() {
		this.station.setState(new MockFullStationState2(this.station));
		this.station.notifyCenter();
		System.out.println(this.station);
	}

	@Override
	public void stable() {
		//
	}

	@Override
	public void update(List<Station<Vehicle>> stations) {
		System.out.println(this);
	}
	
	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is stable";
	}

}
