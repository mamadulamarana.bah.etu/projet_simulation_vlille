package vlille.center;


import static org.junit.jupiter.api.Assertions.*;


import java.util.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import vlille.station.BikeStation;
import vlille.station.MockBikeStation;
import vlille.station.Station;
import vlille.station.util.EmptyStationException;
import vlille.station.util.EmptyStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.UserCannotRentException;
import vlille.user.Mechanic;
import vlille.user.User;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.Bike;
import vlille.vehicle.bike.BikeIsNotAvailableException;
import vlille.vehicle.bike.ClassicBike;
import vlille.vehicle.bike.ElectricBike;
import vlille.vehicle.util.AvailableState;
import vlille.vehicle.util.OutOfServiceState;

public class CenterTest {
	
	protected Center center;

	@BeforeEach
	void init() {
		this.center = Center.getInstance();
	}
	
	@AfterEach
	public void tearDown() {
		this.center.reinitializeCenter();
	}
	
	@Test
	public void stationIsAdedToCenter() {
		assertTrue(this.center.getStations().isEmpty());
		center.addStation(new BikeStation(1));
		assertEquals(this.center.getStations().size(), 1);		
	}
	
	@Test
	public void testThereIsOnlyOneInstanceOfCenter() {
		Station<Vehicle> station = new BikeStation(1);
		station.setCenter(Center.getInstance());
		assertEquals(station.getCenter(), this.center);
	}
	
	@Test
	public void testIfStationIsEmptyOrFullHeIsOnWaitingQueue() throws StationFullException {
		assertEquals(this.center.getStationsBeUpdated().size(), 0);
		Station<Vehicle> st1 = new MockBikeStation(1, 2); st1.setCenter(center);
		assertEquals(this.center.getStationsBeUpdated().size(), 1);
		st1.add(new ClassicBike(1));
		assertEquals(this.center.getStationsBeUpdated().size(), 0);	
		st1.add(new ClassicBike(2));
		assertEquals(this.center.getStationsBeUpdated().size(), 1);
	}
	
	@Test
	public void testIfStationIsStableHeIsNotOnWaitingQueue() throws StationFullException{
		Station<Vehicle> st1 = new MockBikeStation(1, 2); st1.setCenter(center);
		assertEquals(this.center.getStationsBeUpdated().size(), 1);
		st1.add(new ClassicBike(1));
		assertEquals(this.center.getStationsBeUpdated().size(), 0);	
	}
	
	@Test
	public void updateStationsWhichAreEmpty(){
		try {
			Station<Vehicle> st1 = new MockBikeStation2(1, 5); st1.setCenter(center);
			Station<Vehicle> st2 = new MockBikeStation2(2, 15); st2.setCenter(center);
			this.fill(st1, 5); this.fill(st2, 5);
			assertEquals(this.center.getStationsBeUpdated().size(), 1);
			assertEquals(this.center.getStationsBeUpdated().get(0), st1);
			st1.transferAllAvailableRentedTo(st2);
			assertEquals(this.center.getStationsBeUpdated().size(), 1);
			assertEquals(this.center.getStationsBeUpdated().get(0), st1);
			this.center.updateStation(); /**le test et les mocks sont faits
										   de tel sorte qu'il prenne seulement 3 vehicule (voir Mock)*/
			assertEquals(st1.getOccupiedSlot(), 3);
			assertEquals(st2.getOccupiedSlot(), 7);
		}catch(StationFullException|EmptyStationException e) {
			fail();
		}
	}
	
	@Test
	public void updateStationWhichAreFull() throws StationFullException {
		Station<Vehicle> st1 = new MockBikeStation2(1, 5); st1.setCenter(center);
		Station<Vehicle> st2 = new MockBikeStation2(2, 15); st2.setCenter(center);
		this.fill(st1, 5); this.fill(st2, 5);
		assertEquals(this.center.getStationsBeUpdated().size(), 1);
		assertEquals(this.center.getStationsBeUpdated().get(0), st1);
		this.center.updateStation(); /**le test et les mocks sont faits
		   								de tel sorte qu'il prenne seulement 3 vehicule (voir Mock)*/
		assertEquals(st1.getOccupiedSlot(), 0);
		assertEquals(st2.getOccupiedSlot(), 10);
	}
		
	/**
	 * method utilitaires
	 * @param st a station
	 * @return a filled station with 5 vehicle
	 */
	private Station<Vehicle> fill(Station<Vehicle> st, int nbVehicles) throws StationFullException{
		for(int i=0; i<nbVehicles; i++) {
			st.add(new ClassicBike(st.getId()+(i*2) )); /** le (i*2) est là pour varier unpeu les ids des vehicles*/	
		}
		return st;
	}
	
	/**
	 * a vehicle is included in the list of vehicles to be reconditioned if
	 * and only if it is out of service after a rental.
	 * @throws BikeIsNotAvailableException if the bike is outOfService after rent
	 */
	@Test
	public void testToUpdateVehicleWhenItIsOutOfService() throws BikeIsNotAvailableException {
		try {
			Vehicle v1 = new ElectricBike(1); v1.setCenter(this.center);
			Vehicle v2 = new ClassicBike(2); v1.setCenter(this.center);
			this.center.setMechanic(new Mechanic());
			assertEquals(this.center.getVehicleToBeReconditionned().size(), 0);
			v1.setNbUse(1);
			v1.rent(); /**rent v1*/
			v1.available(); /**return v1*/
			assertEquals(this.center.getVehicleToBeReconditionned().size(), 1);
			assertEquals(this.center.getVehicleToBeReconditionned().get(0), v1);
			v2.rent();
			assertEquals(this.center.getVehicleToBeReconditionned().size(), 1);
			assertTrue(v1.getState() instanceof OutOfServiceState);
			this.center.updateVehicles();
			assertTrue(v1.getState() instanceof AvailableState);
		}catch(BikeIsNotAvailableException e) {
			fail();
		}
	}

}