package vlille.center;

import java.util.List;

import java.util.Random;

import vlille.station.Station;
import vlille.station.util.EmptyStationException;
import vlille.station.util.StableStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;

public class MockEmptyStationState2 extends StationState{

	public MockEmptyStationState2(Station<Vehicle> station) {
		super(station);
	}

	@Override
	public void empty() {
		//
	}

	@Override
	public void full() {
		this.station.setState(new MockFullStationState2(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void stable() {
		this.station.setState(new MockStableStationState2(this.station));
		System.out.println("je suis ici");
		this.station.notifyCenter();
		System.out.println(this.station);
	}
	
	/**le mock est fait pour nbTakenVehicle et index qui est de base aléatoire*/
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = 1;
		int nbTakenVehicle = 3;
		while(nbTakenVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station)) && (choosenStation.getOccupiedSlot()>0) ) {
					if (nbTakenVehicle == choosenStation.getOccupiedSlot()) {
						nbTakenVehicle = choosenStation.transferAllAvailableRentedTo(this.station);
					}
					else {
						nbTakenVehicle = choosenStation.transferNbVehicleToanotherStation(this.station, nbTakenVehicle);
					}
				}
				index = 1;
			}
			catch(EmptyStationException e) {
				index = 1;
			}catch(StationFullException e) {
				System.out.println(this.station);
			}
		}
		this.stable();
	}
	
	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is empty";
	}
}
