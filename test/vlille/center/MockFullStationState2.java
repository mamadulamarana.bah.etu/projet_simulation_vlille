package vlille.center;

import java.util.List;
import java.util.Random;

import vlille.station.Station;
import vlille.station.util.EmptyStationException;
import vlille.station.util.EmptyStationState;
import vlille.station.util.StableStationState;
import vlille.station.util.StationFullException;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class MockFullStationState2 extends StationState{

	public MockFullStationState2(Station<Vehicle> station) {
		super(station);
	}
	
	@Override
	public void empty() {
		this.station.setState(new MockEmptyStationState2(this.station));
	}

	@Override
	public void full() {
		//
	}
	
	@Override
	public void stable() {
		this.station.setState(new MockStableStationState2(this.station));
		this.station.notifyCenter();
		System.out.println(this.station);
	}
	
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = 1; // le mock est fait pour tester l'aléatoire ici
		int nbTransferedVehicle = 5; // et là
		System.out.println("ici");
		while(nbTransferedVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station))&& (choosenStation.getFreeSlot()> 0) ) {
					if (nbTransferedVehicle == choosenStation.getFreeSlot()) {
						nbTransferedVehicle = this.station.transferAllAvailableRentedTo(choosenStation);
					}
					else {
						nbTransferedVehicle = this.station.transferNbVehicleToanotherStation(choosenStation, nbTransferedVehicle);
					}
				}
				index = 1;
			}catch(StationFullException e) {
				index = 1;
			}catch(EmptyStationException e) {
				System.out.println(this.station);
			}
		}
		if(this.station.isEmpty()) {
			this.empty();
		}
		else {
			this.stable();
		}
	}

	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is full";
	}
	
}
