package vlille.center;

import java.util.*;


import vlille.station.Station;
import vlille.station.util.StationFullException;
import vlille.user.AbstractVisitor;
import vlille.user.Mechanic;
import vlille.user.Visitor;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.Bike;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class Center implements Observer{
	
	private static Center uniqueInstance;
	private static final int DEFAULT_TIME_INTERVAL = 2;
	private Mechanic visitor;
	private int intervalle;
	
	private List<Station<Vehicle>> stations;
	private List<Station<Vehicle>> stationsBeUpdated;
	private List<Vehicle> vehicleToBeReconditionned;
	
	/**
	 * build an instance of center
	 */
	private Center() {
		this.stations = new ArrayList<>();
		this.visitor = null;
		this.vehicleToBeReconditionned = new ArrayList<Vehicle>();
		this.stationsBeUpdated =new ArrayList<>();
		this.intervalle = DEFAULT_TIME_INTERVAL;
	}
	
	/**
	 * define a single center instance
	 * center that manages the entire fleet 
	 * @return a new instance of center, or a unique instance which already initialized
	 */
	public static Center getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new Center();
		}
		return uniqueInstance;
	}
	
	/**
	 * add a station to the observer control
	 * @param station
	 */
	public void addStation(Station<Vehicle> station) {
		if (!this.stations.contains(station)) {
			this.stations.add(station);
		}
	}
	
	/**
	 * set a time interval at the end of which you have redistribute vehicle
	 * @param timeIntervalle the given time interval
	 */
	public void setTimeInterval(int timeIntervalle) {
		this.intervalle = timeIntervalle;
	}
	
	/**
	 * get the defined time interval
	 * @return the defined time interval
	 */
	public int getInterval() {
		return this.intervalle;
	}
	
	/**
	 * remove a station from the observer control
	 * @param station a station which we want to remove
	 */
	public void removeStation(Station<Vehicle> station) {
		if (this.stations.contains(station)) {
			this.stations.remove(station);
		}
	}
	
	/**
	 * remove all stations (method utilitaires) to reinitialize center
	 */
	public void reinitializeCenter() {
		this.stations.removeAll(this.stations);
		this.vehicleToBeReconditionned.removeAll(this.vehicleToBeReconditionned);
		this.stationsBeUpdated.removeAll(this.stationsBeUpdated);
	}
	
	/**
	 * get the stations to be updated after time interval
	 * @return stations to be updated
	 */
	public List<Station<Vehicle>> getStationsBeUpdated() {
		return this.stationsBeUpdated;
	}
	
	/**
	 * set a mechanic for this center, to control a out of service vehicles
	 * @param mechanic
	 */
	public void setMechanic(Mechanic mechanic) {
		this.visitor = mechanic;
	}
	
	/**
	 * get the vehicles to be reconditionned after a time interval
	 * @return
	 */
	public List<Vehicle> getVehicleToBeReconditionned() {
		return this.vehicleToBeReconditionned;
	}

	/**
	 * Get list of station
	 * @return the list of station
	 */
	public List<Station<Vehicle>> getStations() {
		return this.stations ;
	}
	
	@Override
	public void addObjectToBeUpdate(Station<Vehicle> station) {
		if ( this.stationsBeUpdated.contains(station)) {
			this.stationsBeUpdated.remove(station);
		}
		else {
			this.stationsBeUpdated.add(station);
		}
	}
	
	/**
	 * update all station which that are empty or full after a defined interval
	 */
	public void updateStation() {
		for (Station<Vehicle> station: this.stationsBeUpdated) {
			station.update(this.stations);
		}
	}
	
	public void updateVehicles() throws BikeIsNotAvailableException {
		for (Vehicle v: this.vehicleToBeReconditionned) {
			v.available();
		}
	}
	
	/**
	 * reconditioning repaired vehicles after a defined time interval
	 */
	public void restoreReconditionedVehicle() {
		
	}

	@Override
	public void sendMechanic(Vehicle vehicle) {
		this.visitor.fixVehicle(vehicle);
		this.vehicleToBeReconditionned.add(vehicle);
	}
	

	/**
	 * description of a center
	 */
	public String toString() {
		return "unique center";
	}
}

