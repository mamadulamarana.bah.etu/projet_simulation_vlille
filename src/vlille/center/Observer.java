package vlille.center;

import java.util.List;

import vlille.station.Station;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;

public interface Observer {
	
	/**
	 * 
	 */
	public void addObjectToBeUpdate(Station<Vehicle> station);
	
	/**
	 * 
	 * @param vehicle
	 */
	public void sendMechanic(Vehicle vehicle);
	
	/**
	 * 
	 * @param station
	 */
	public void addStation(Station<Vehicle> station);
	
	/**
	 * remove a station from the observer control
	 * @param station
	 */
	public void removeStation(Station<Vehicle> station);
	
	/**
	 * 
	 */
	public void reinitializeCenter();
	
	/**
	 * 
	 * @return
	 */
	public List<Station<Vehicle>> getStations();
}
