package vlille.vehicle;

import vlille.center.Center;

import vlille.center.Observer;
import vlille.station.Station;
import vlille.station.util.UserCannotRentException;
import vlille.user.AbstractVisitor;
import vlille.user.User;
import vlille.user.Visitor;
import vlille.vehicle.bike.Bike;
import vlille.vehicle.bike.BikeIsNotAvailableException;
import vlille.vehicle.util.AvailableState;
import vlille.vehicle.util.VehicleState;

/**
 * 
 * @author mamad loic
 * class of Vehicle
 *
 */
public abstract class Vehicle implements Rentable {

	protected static final int NB_Of_Use_Default = 10;
	protected static final int AGE = 20; /**default age for location a vehicle*/
	protected int nbOfUse;
	protected int id;
	protected String type;
	protected VehicleState state; /** represent the state of the vehicle*/
	protected Observer observer;
	
	/**
	 * build a vehicle
	 */
	public Vehicle() {
		this.type = "Vehicle"; /** vehicle has "vehicle" as a description by default*/
		this.state = new AvailableState(this);
		this.nbOfUse = Vehicle.NB_Of_Use_Default;
		this.id = 0; /** vehicle has an id equal 0 by default*/
	}
	
	/**
	 * accept an exterior visitor for doing something on this vehicle
	 * @param visitor who wants to visit this vehicle
	 */
	public abstract void accept(AbstractVisitor visitor);
	
	/**
	 * set a new state for this vehicle
	 * @param state the new state
	 */
	public void setState(VehicleState state) {
		this.state = state;
	}
	
	public void setNbUse(int nbUse) {
		this.nbOfUse = nbUse;
	}
	
	/**
	 * get the current state for this vehicle
	 * @return the current state
	 */
	public VehicleState getState() {
		return this.state;
	}
				
	/**
	 * stole a vehicle
	 */
	public void stoleVehicle() {
		
	}
			
	/**
	 * rent a vehicle
	 */
	public void rent() throws BikeIsNotAvailableException{
		this.state.rent();
		this.usedOneTime();
	}

	/**
	 * the vehicle is out of service, it's need to be repair
	 * @throws BikeIsNotAvailableException 
	 */
	public void outOfService() throws BikeIsNotAvailableException {
		this.state.outOfService();
	}

	/**
	 * stole a vehicle
	 * @throws BikeIsNotAvailableException 
	 */
	public void stole() throws BikeIsNotAvailableException {
		this.state.stole();
	}

	/**
	 * make this vehicle available
	 * @throws BikeIsNotAvailableException 
	 */
	public void available() throws BikeIsNotAvailableException {
		this.state.available();
	}
	
	/**
	 * 
	 * @throws BikeIsNotAvailableException 
	 */
	public void transfer() throws BikeIsNotAvailableException {
		this.state.transfer();
	}
	
	/**
	 * get the type of this vehicle
	 * @return the current type
	 */
	public String getType() {
		return this.type;
	}
	
	/**
	 * Get the Id
	 * @return int id, the id of the vehicle
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Get the nb of use
	 * @return the nbOfUse
	 */
	public int getNbOfUse() {
		return this.nbOfUse;
	}
	
	/**
	 * Know if an object can be transfered
	 * @return true if it can, false otherwise
	 */
	public boolean canTransferObject(){
		try {
			this.transfer();
			return true;
		}catch(BikeIsNotAvailableException e) {
			return false;
		}
	}
	
	/**
	 * set the id of the vehicle
	 * @param id the new id of the vehicle
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * add the station on the control of a observer
	 * @param observer
	 */
	public void setCenter(Observer center) {
		this.observer = center;
	}
	
	/**
	 * notify the center to send someone to check the vehicle
	 */
	public void notifyCenter() {
		this.observer.sendMechanic(this);
	}
	
	/** (a tester)
	 * set the number of utilization max of the vehicle before reparation
	 */
	public void control() {
		this.nbOfUse = Vehicle.NB_Of_Use_Default;
	}


	/**
	 * use the vehicle one time, and change to out of service if needed
	 * @throws BikeIsNotAvailableException
	 */
	public void usedOneTime() throws BikeIsNotAvailableException {
		if (this.nbOfUse > 0) {
			this.nbOfUse -= 1;
		}
	}
	
	/**
	 * know if the user can rent an object
	 * @return true if he can, false otherwise
	 * @throws BikeIsNotAvailableException 
	 * @throws UserCannotRentException 
	 */
	public boolean canRentObject(User user) throws BikeIsNotAvailableException {
		if (user.getAge() >= Vehicle.AGE) {
			this.rent();
			return true;
		}
		return false;
	}
	
	/**
	 * displays a message to indicate vehicle status
	 * @param msg the message to be displayed 
	 */
	public void displayStateOfVehicle(Vehicle v) {
		System.out.println(v);
	}

		
	/**
	 * to compare two vehicle objects
	 */
	public boolean equals(Object o) {
		if(!(o instanceof Vehicle) ) {
			return false;
		}
		Vehicle other =(Vehicle)o;
		return (this.type.equals(other.getType())) && (this.id == other.id);
	}
	
	/**
	 * Describe a vehicle
	 */
	public String toString() {
		return this.getState()+"";
	}

}