package vlille.vehicle;

import vlille.station.util.UserCannotRentException;
import vlille.user.User;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public interface Rentable {
	
	/**
	 * know if an object is rentable
	 * @return True if the object can be rented by some user, otherwise false
	 * @throws BikeIsNotAvailableException 
	 * @throws UserCannotRentException 
	 */
	public boolean canRentObject(User u) throws BikeIsNotAvailableException, UserCannotRentException;
	
	/**
	 * 
	 * @return
	 * @throws BikeIsNotAvailableException 
	 */
	public boolean canTransferObject();
	
}
