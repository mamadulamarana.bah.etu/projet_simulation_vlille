package vlille.vehicle.util;

import vlille.vehicle.Vehicle;

public class AvailableState extends VehicleState{

	/**
	 * Constructor
	 * @param vehicle
	 */
	public AvailableState(Vehicle vehicle) {
		super(vehicle);
	}

	/**
	 * this vehicle is now stolen
	 */
	public void stole() {
		this.vehicle.setState(new StolenState(this.vehicle));
		System.out.println("ooops, Vehicle is stolen");
		//this.vehicle.stoleVehicle();
	}

	/**
	 * this vehicle is now rented by someone
	 */
	public void rent() {
		this.vehicle.setState(new RentedState(this.vehicle));
		System.out.println("Vehicle is Rented by someone");
	}

	/**
	 * this vehicle is now out of service, can't be use before reparation by "Mechanic"
	 */
	public void outOfService(){
		this.vehicle.setState(new OutOfServiceState(this.vehicle));
		System.out.println("Vehicle is Out of Service, he need to be repair");
	}

	/**
	 * Vehicle is available to rent
	 */
	public void available() {
		System.out.println("Vehicle is already available");
	}
	
	/**
	 * transfer of a vehicle to an other station
	 */
	public void transfer() {
		System.out.println("this vehicle can be send to another station");
	}
	
	/**
	 * 
	 */
	public String toString() {
		return this.vehicle.getType()+" "+this.vehicle.getId()+ " is available";
	}

}
