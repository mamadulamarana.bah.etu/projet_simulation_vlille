package vlille.vehicle.util;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class RentedState extends VehicleState {

	/**
	 * 
	 * @param vehicle
	 */
	public RentedState(Vehicle vehicle) {
		super(vehicle);
	}

	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void stole() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("vehicle is currently rented, it's can't be stolen");
	}

	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void rent() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("vehicle is already rented");
	}

	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void outOfService() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("vehicle is currently rented");
	}
	
	/**
	 * 
	 */
	public void transfer() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("this vehicle is rented, it can't be send to another station");
	}

	/**
	 * make the bike available again after a rental if it can still be used,
	 * or put it out of service and call a mechanic for a check-up.
	 */
	public void available() {
		if (this.vehicle.getNbOfUse() == 0) {
			this.vehicle.setState(new OutOfServiceState(this.vehicle));
			this.vehicle.notifyCenter();
			this.vehicle.displayStateOfVehicle(this.vehicle);
		}
		else {
			this.vehicle.setState(new AvailableState(this.vehicle));
			this.vehicle.displayStateOfVehicle(this.vehicle);
		}		
	}
	
	/**
	 * 
	 */
	public String toString() {
		return this.vehicle.getType()+" "+this.vehicle.getId()+" is rented";
	}

}
