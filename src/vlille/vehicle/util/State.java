package vlille.vehicle.util;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

/**
 * state of vehicle object
 * state represents if vehicle is in state of repair, stolen or rented
 */
public abstract class State {
	
	protected Vehicle vehicle;
	
	/**
	 * Constructor
	 * @param vehicle
	 */
	public State(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	public abstract void stole() throws BikeIsNotAvailableException;
	public abstract void rent() throws BikeIsNotAvailableException;
	public abstract void outOfService() throws BikeIsNotAvailableException;
	public abstract void available() throws BikeIsNotAvailableException;
	public abstract void transfer() throws BikeIsNotAvailableException;
}
