package vlille.vehicle.util;

import vlille.station.util.UserCannotRentException;
import vlille.user.AbstractVisitor;
import vlille.user.User;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

/**
 * class VehicleDecorator for decoration
 * @param decoration
 */
public abstract class VehicleDecorator extends Vehicle{

	protected Vehicle vehicle;
	
	/**
	 * build a VehicleDecorator
	 * @param decoration
	 */
	public VehicleDecorator(Vehicle vehicle) {
		super();
		this.vehicle = vehicle;
	}
	
	public String getType() {
		return this.vehicle.getType();
	}
	
	/**
	 * 
	 */
	public void accept(AbstractVisitor visitor) {
		visitor.visit(this);
	}

}
