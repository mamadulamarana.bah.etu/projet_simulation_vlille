package vlille.vehicle.util;

import vlille.vehicle.Vehicle;

public class Rack extends VehicleDecorator {

	/**
	 * Builda decoration with Rack for a vehicle
	 * @param decoration
	 */
	public Rack(Vehicle vehicle) {
		super(vehicle);
	}
	
	/**
	 * decorate the vehicle with Rack
	 */
	public String getType() {
		return super.getType() + " with Rack";
	}
	
}
