package vlille.vehicle.util;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class StolenState extends VehicleState {

	/**
	 * 
	 * @param vehicle
	 */
	public StolenState(Vehicle vehicle) {
		super(vehicle);
	}

	/**
	 * 
	 */
	public void stole() {
		this.vehicle.displayStateOfVehicle(this.vehicle);
	}

	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void rent() throws BikeIsNotAvailableException{
		throw new BikeIsNotAvailableException("Vehicle has been stolen");
	}

	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void transfer() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("this vehicle is stolen, he can't be send to another station");
	}
	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void outOfService() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("Vehicle has been stolen");
	}
	
	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void available() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("Vehicle has been stolen");
	}
	
	/**
	 * 
	 */
	public String toString() {
		return this.vehicle.getType()+" "+this.vehicle.getId()+" is stolen";
	}

}
