package vlille.vehicle.util;

import vlille.vehicle.Vehicle;

public class Basket extends VehicleDecorator {

	/**
	 * build a decoration with basket for a vehicle
	 * @param decoration
	 */
	public Basket(Vehicle vehicle) {
		super(vehicle);
	}
	
	/**
	 * decorate the vehicle with VehicleWithBasket
	 */
	public String getType() {
		return super.getType() + " with basket";
	}

}
