package vlille.vehicle.util;

import vlille.vehicle.Vehicle;

public class VehicleWithRack extends VehicleDecorator {

	/**
	 * Builda decoration with Rack for a vehicle
	 * @param decoration
	 */
	public VehicleWithRack(Vehicle vehicle) {
		super(vehicle);
	}
	
	/**
	 * decorate the vehicle with Rack
	 */
	public String getType() {
		return super.getType() + " with rack";
	}
	
}
