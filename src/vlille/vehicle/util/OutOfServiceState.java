package vlille.vehicle.util;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class OutOfServiceState extends VehicleState{

	/**
	 * 
	 * @param vehicle
	 */
	public OutOfServiceState(Vehicle vehicle) {
		super(vehicle);
	}
	
	/**
	 * @throws BikeIsNotAvailableException 
	 * 
	 */
	public void stole() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("Vehicle is out of service, it's can't be stolen");
	}

	/**
	 * 
	 * @throws
	 */
	public void rent() throws BikeIsNotAvailableException {
		throw new BikeIsNotAvailableException("Vehicle is out of service, it's can be rented");
	}

	/**
	 * 
	 */
	public void outOfService() {
		this.vehicle.displayStateOfVehicle(this.vehicle);
	}
	
	/**
	 * 
	 */
	public void transfer() throws BikeIsNotAvailableException{
		throw new BikeIsNotAvailableException("this vehicle cannot be send to another station, it need to be repaired");
	}
	
	/**
	 * 
	 */
	public void available() {
		this.vehicle.setState(new AvailableState(this.vehicle));
		System.out.println("Vehicle is available again");
	}
	
	/**
	 * 
	 */
	public String toString() {
		return this.vehicle.getType()+" "+this.vehicle.getId()+" is out of service";
	}

}
