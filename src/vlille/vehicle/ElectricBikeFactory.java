package vlille.vehicle;

import vlille.vehicle.bike.ElectricBike;

public class ElectricBikeFactory implements VehicleFactory{

	public ElectricBikeFactory() {}

	/**
	 * create an electric bike
	 * @param id the id of the vehicle
	 * @return created vehicle
	 */
	public Vehicle createVehicle(int id) {
		return new ElectricBike(id);
	}

}
