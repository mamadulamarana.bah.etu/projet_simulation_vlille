package vlille.vehicle.bike;

public class BikeIsNotAvailableException extends Exception {

	public BikeIsNotAvailableException(String msg) {
		super(msg);
	}
}
