package vlille.vehicle.bike;

import vlille.user.AbstractVisitor;
import vlille.user.User;
import vlille.vehicle.Rentable;

public class ElectricBike extends Bike {
	
	/**
	 * build a electric Vehicle
	 * @param id TODO
	 */
	public ElectricBike(int id) {
		super(id);
		this.type = "Electric bike";
	}

	/**
	 * accept to visit an electric bike
	 */
	public void accept(AbstractVisitor visitor) {
		visitor.visit(this);
	}
	
}
