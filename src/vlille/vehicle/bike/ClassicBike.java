package vlille.vehicle.bike;

import vlille.user.AbstractVisitor;
import vlille.user.User;
import vlille.vehicle.Rentable;

public class ClassicBike extends Bike {
	
	/**
	 * build a classic vehicle
	 * @param id TODO
	 */
	public ClassicBike(int id) {
		super(id);
		this.type = "Classic bike";
	}

	/**
	 * accept a visitor for a classicbike
	 */
	public void accept(AbstractVisitor visitor) {
		visitor.visit(this);
	}
	
}
