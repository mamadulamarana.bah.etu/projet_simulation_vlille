package vlille.vehicle.bike;

import vlille.station.util.UserCannotRentException;

import vlille.user.User;
import vlille.vehicle.Vehicle;

public abstract class Bike extends Vehicle{
	
	protected static final int AGE = 18;

	/**
	 * Constructor
	 * @param id TODO
	 */
	public Bike(int id) {
		super();
		this.id = id;
	}
	
	/**
	 * know if the user can rent an object
	 * @return true if he can, false otherwise
	 * @throws BikeIsNotAvailableException 
	 * @throws UserCannotRentException 
	 */
	public boolean canRentObject(User user) throws BikeIsNotAvailableException {
		if (user.getAge() >= Bike.AGE) {
			this.rent();
			return true;
		}
		return false;
	}

}
