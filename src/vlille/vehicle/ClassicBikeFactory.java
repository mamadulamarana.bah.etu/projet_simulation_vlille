package vlille.vehicle;

import vlille.vehicle.bike.ClassicBike;

public class ClassicBikeFactory implements VehicleFactory{

	public ClassicBikeFactory() {}

	/**
	 * create an classic bike
	 * @param id the id of the vehicle
	 * @return created vehicle
	 */
	public Vehicle createVehicle(int id) {
		return new ClassicBike(id);
	}
	
	

}
