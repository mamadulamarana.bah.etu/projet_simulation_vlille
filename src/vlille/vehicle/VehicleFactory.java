package vlille.vehicle;

public interface VehicleFactory {

	public Vehicle createVehicle(int id);
}
