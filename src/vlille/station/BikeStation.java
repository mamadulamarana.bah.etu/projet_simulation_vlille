package vlille.station;

import vlille.center.Center;
import vlille.center.Observer;
import vlille.station.util.EmptyStationState;
import vlille.station.util.StableStationState;
import vlille.station.util.StationState;
import vlille.vehicle.Vehicle;

public class BikeStation extends Station<Vehicle>{

	/**
	 * build a station with initial state and his Center
	 * @param id
	 */
	public BikeStation(int id) {
		super(id);
		this.state = new EmptyStationState(this);
	}
	
	@Override
	public void setState(StationState newState) {
		this.state = newState;
	}
	
	@Override
	public void notifyCenter() {
		this.observer.addObjectToBeUpdate(this);
	}
	
	@Override
	public void setCenter(Observer observer) {
		this.observer = observer;
		this.observer.addStation(this);	
		this.notifyCenter();
	}

	/**
	 * 
	 */
	public String toString() {
		return "Bike "+this.getState();
	}

}
