package vlille.station.util;

import java.util.List;

import vlille.station.Station;
import vlille.vehicle.Vehicle;

public abstract class StationState {

	protected Station<Vehicle> station;
	
	/**
	 * 
	 * @param station
	 */
	public StationState(Station<Vehicle> station) {
		this.station = station;
	}
	
	/**
	 * 
	 */
	public abstract void empty();
	
	/**
	 * 
	 */
	public abstract void full();
	
	/**
	 * 
	 */
	public abstract void stable();
	
	/**
	 * 
	 */
	public abstract void update(List<Station<Vehicle>> stations);

}
