package vlille.station.util;

import java.util.List;

import vlille.station.Station;
import vlille.vehicle.Vehicle;

public class StableStationState extends StationState {

	public StableStationState(Station<Vehicle> station) {
		super(station);
	}

	@Override
	public void empty() {
		this.station.setState(new EmptyStationState(this.station));
		this.station.notifyCenter();
		System.out.println(this.station);
	}

	@Override
	public void full() {
		this.station.setState(new FullStationState(this.station));
		this.station.notifyCenter();
		System.out.println(this.station);
	}

	@Override
	public void stable() {
		//
	}

	@Override
	public void update(List<Station<Vehicle>> stations) {
		System.out.println(this);
	}
	
	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is stable";
	}

}
