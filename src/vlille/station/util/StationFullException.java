package vlille.station.util;

public class StationFullException extends Exception {

	public StationFullException(String msg) {
		super(msg);
	}
}
