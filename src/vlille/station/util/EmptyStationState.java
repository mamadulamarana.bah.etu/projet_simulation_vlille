package vlille.station.util;

import java.util.List;
import java.util.Random;

import vlille.station.Station;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class EmptyStationState extends StationState{

	public EmptyStationState(Station<Vehicle> station) {
		super(station);
	}

	@Override
	public void empty() {
		//
	}

	@Override
	public void full() {
		this.station.setState(new FullStationState(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void stable() {
		this.station.notifyCenter();
		this.station.setState(new StableStationState(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = new Random().nextInt(0, stations.size()); // un nombre entre 0 et size de stations
		int nbTakenVehicle = new Random().nextInt(0, this.station.getCapacity()); // un nombre entre 0 et capacity de station
		while(nbTakenVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station))&& choosenStation.getOccupiedSlot()>0) {
					if (nbTakenVehicle == choosenStation.getOccupiedSlot()) {
						nbTakenVehicle = choosenStation.transferAllAvailableRentedTo(this.station);
					}
					else {
						nbTakenVehicle = choosenStation.transferNbVehicleToanotherStation(this.station, nbTakenVehicle);
					}
				}
				index = new Random().nextInt(0, stations.size());
			}
			catch(EmptyStationException e) {
				index = new Random().nextInt(0, stations.size());
			}catch(StationFullException e) {
				System.out.println(this.station);
			}
		}
		this.stable();
	}
	
	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is empty";
	}


}
