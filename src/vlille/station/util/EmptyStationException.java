package vlille.station.util;

public class EmptyStationException extends Exception {

	public EmptyStationException(String message) {
		super(message);
	}
}
