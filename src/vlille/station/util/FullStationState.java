package vlille.station.util;

import java.util.List;
import java.util.Random;

import vlille.station.Station;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class FullStationState extends StationState{

	public FullStationState(Station<Vehicle> station) {
		super(station);
	}

	@Override
	public void empty() {
		this.station.setState(new EmptyStationState(this.station));
	}

	@Override
	public void full() {
		//
	}
	
	@Override
	public void stable() {
		this.station.notifyCenter();
		this.station.setState(new StableStationState(this.station));
		System.out.println(this.station);
	}
	
	@Override
	public void update(List<Station<Vehicle>> stations) {
		int index = new Random().nextInt(0, stations.size()); // un nombre entre 0 et size de stations
		int nbTransferedVehicle = new Random().nextInt(0, this.station.getCapacity()); // un nombre entre 0 et capacity de station
		while(nbTransferedVehicle > 0) {
			try {
				Station<Vehicle> choosenStation = stations.get(index);
				if ( (!choosenStation.equals(this.station))&& (choosenStation.getFreeSlot()> 0) ) {
					if (nbTransferedVehicle == choosenStation.getFreeSlot()) {
						nbTransferedVehicle = this.station.transferAllAvailableRentedTo(choosenStation);
					}
					else {
						nbTransferedVehicle = this.station.transferNbVehicleToanotherStation(choosenStation, nbTransferedVehicle);
					}
				}
				index = new Random().nextInt(0, stations.size());
			}catch(StationFullException e) {
				index = new Random().nextInt(0, stations.size());
			}catch(EmptyStationException e) {
				System.out.println(this.station);
			}
		}
		if(this.station.isEmpty()) {
			this.empty();
		}
		else {
			this.stable();
		}
	}

	/**
	 * Description of a station with his state
	 */
	public String toString() {
		return "station "+ this.station.getId() +" is full";
	}
	
}
