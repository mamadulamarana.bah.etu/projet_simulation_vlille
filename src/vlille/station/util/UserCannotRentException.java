package vlille.station.util;

public class UserCannotRentException extends Exception {
	
	public UserCannotRentException(String msg) {
		super(msg);
	}

}
