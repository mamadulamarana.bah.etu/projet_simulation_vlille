package vlille.station;

import java.util.*;

import vlille.center.*;
import vlille.center.Observer;
import vlille.station.util.*;
import vlille.user.User;
import vlille.vehicle.*;
import vlille.vehicle.bike.BikeIsNotAvailableException;

/**
 * 
 * @param <T>
 */
public abstract class Station<T extends Rentable>{
	
	protected int id;
	protected static final int CAPACITY_MIN = 10;
	protected static final int CAPACITY_MAX = 21;
	protected int capacity;
	protected int occupiedSlot;
	protected List<T> available;
	protected Observer observer;
	protected StationState state;
	
	/**
	 * Constructor
	 * @param id
	 */
	public Station(int id) {
		this.id = id;
		this.capacity = this.setCapacity();
		this.available = new ArrayList<T>();
		this.occupiedSlot = 0;
		this.observer = null;
		this.state = null;
	}

	/**
	 * the capacity of station
	 * @return capacity of station
	 */
	public int getCapacity() {
		return this.capacity;
	}
	
	/**
	 * set the capacity of the station
	 * @return capacity
	 */
	private int setCapacity() {
		Random r = new Random();
		int capacity = r.nextInt(Station.CAPACITY_MIN, Station.CAPACITY_MAX);
		return capacity;
	}
	
	/**
	 * set a new state for this station
	 * the state of a station is on empty
	 */
	public abstract void setState(StationState state);
	
	/**
	 * get a description of a station state
	 * @return
	 */
	public StationState getState() {
		return this.state;
	}
	
	/**
	 * @throws StationFullException 
	 * 
	 */
	public void empty() {
		this.state.empty();
	}
	
	/**
	 * @throws EmptyStationException 
	 * 
	 */
	public void full() {
		this.state.full();
	}
	
	/**
	 * 
	 */
	public void stable() {
		this.state.stable();
	}
	
	/**
	 * 
	 * @param stations
	 */
	public void update(List<Station<Vehicle>> stations) {
		this.state.update(stations);
	}

	
	/**
	 * get the occupied slot of station
	 * @return the occupied slot
	 */
	public int getOccupiedSlot() {
		return this.occupiedSlot;
	}
	
	public int getFreeSlot() {
		return this.getCapacity() - this.occupiedSlot;
	}
	
	/**
	 * add the station on the control of a observer
	 * @param observer
	 */
	public abstract void setCenter(Observer observer);
	
	/**
	 * get the center of this station
	 * @return the center of this station
	 */
	public Observer getCenter() {
		return this.observer;
	}
	
	/**
	 * get the available rentable object
	 * @return the available rentable object
	 */
	public List<T> getAvailable() {
		return this.available;
	}
	
	/**
	 * remove one Rentable Object from a station for transfer, use by observer
	 * @param place
	 * @return
	 * @throws StationFullException 
	 */
	public int transferNbVehicleToanotherStation(Station <? super T> renter, int nbTransferedVehicle) throws StationFullException {
		Iterator<T> it = this.available.iterator();
		while ( (nbTransferedVehicle > 0) && it.hasNext()) {
			T vehicle = it.next();
			if (vehicle.canTransferObject()) {
				renter.add(vehicle);
				nbTransferedVehicle--;
				it.remove();
				this.occupiedSlot--;
			}
		}
		if (this.isEmpty()) {
			this.empty();
		}
//		else {
//			this.stable();
//		}
		return nbTransferedVehicle;
	}

	/**
	 * add a rentable object in a station
	 * @param rentable a rentable object
	 * @throws EmptyStationException 
	 */
	public void add(T rentable) throws StationFullException {
		if (this.occupiedSlot < this.capacity) {
			if ( !(this.available.contains(rentable)) ) {
				this.available.add(rentable);
				this.occupiedSlot += 1;
			}
			
			if (this.isFull()) {
				this.full();
			}
			else {
				this.stable();
			}
		}
		else {
			throw new StationFullException(this+"");
		}
	}
	
	/**
	 * to check if this station is full or not
	 * @return true if it's full, false or not
	 */
	public boolean isFull() {
		return this.getCapacity() == this.getOccupiedSlot();
	}
	
	/**
	 * to check if this station is empty or not
	 * @return true if it's full, false or not
	 */
	public boolean isEmpty() {
		return this.getOccupiedSlot() == 0;
	}
	
	/**
	 * add a list of rentable object
	 * @param rented list of a rentable object
	 * @throws StationFullException 
	 */
	public void addList(List<? extends T> rented) throws StationFullException{
		for (T rentable : rented) {
			this.add(rentable);
		}
	}
	
	/**
	 * remove a rentable from the available object
	 * @param rented
	 */
	public void remove(T rented) {
		this.available.remove(rented);
		this.occupiedSlot--;
	}
	
	/**
	 * remove all rentable from the available object
	 */
	public void removeAll() {
		this.available.removeAll(this.available);
		this.occupiedSlot = 0;
	}
	
	/**
	 * rent a decoration to someone
	 * @param user the user who want rent the decoration
	 * @throws UserCannotRentException this exception is thrown if the user can't rent this object
	 * @return rented decoration
	 * @throws EmptyStationException 
	 * @throws BikeIsNotAvailableException 
	 */
	public T rentTo(User user, int index) throws UserCannotRentException, EmptyStationException, BikeIsNotAvailableException{
		if (this.occupiedSlot > 0) {
			T rented = this.getAvailable().get(index);
			if ( !(rented.canRentObject(user)) ) {
				throw new UserCannotRentException(user+", cannot rent this object");
			}
			this.remove(rented);
			if (this.isEmpty()) {
				this.empty();
			}
			else {
				this.stable();
			}
			return rented;
		}
		else {
			throw new EmptyStationException(this.getId()+"station is empty");
		}
	}
	
	
	/**
	 * transfer all rentable object to another renter
	 * @param renter another renter 
	 * @throws StationFullException 
	 * @throws EmptyStationException
	 * @throws BikeIsNotAvailableException
	 * @throws EmptyStationException 
	 */
	public int transferAllAvailableRentedTo(Station <? super T> renter) throws StationFullException, EmptyStationException {
		if(this.occupiedSlot > 0) {
			Iterator<T> it = this.available.iterator();
			while(it.hasNext()) {
				T rented = it.next();
				if(rented.canTransferObject()) {
					renter.add(rented);
					it.remove();
					this.occupiedSlot--;
				}
			}
			if (this.isEmpty()) {
				this.empty();;
			}
//			else {
//				this.stable();
//			}
			return this.occupiedSlot;
		}
		else {
			throw new EmptyStationException(this+"");
		}
	}
	
	/**
	 * adds to a Renter all (rentable) objects in a list
	 * if they are not already managed by this Renter,
	 * and transfers duplicates to another provided Renter
	 * @param goods rentable object
	 * @param other another renter to transfer duplicate objects
	 * @throws StationFullException 	
	 * @throws EmptyStationException 
	 */
	public void addGoodsAndTransferDuplicate
	(List<? extends T> goods, Station<? super T> other) throws StationFullException {
		List<T> duplicate = new ArrayList<>();
		for (T good : goods) {
			if ( !(this.available.contains(good)) ) {
				this.add(good);
			}
			else {
				duplicate.add(good);
			}
		}
		other.addList(duplicate);
	}
	
	/**
	 * notify the observer that something change and need update
	 */
	public abstract void notifyCenter();


	/**
	 * get the id of the station
	 * @return the id of the station
	 */
	public Integer getId() {
		return this.id;
	}
	
	/**
	 * Equals
	 */
	public boolean equals(Object o) {
		if( !(o instanceof Station)) {
			return false;
		}
		Station<T> other = (Station<T>)o;
		return (this.id == other.getId());
	}
	
	/**
	 * description of the Station
	 */
	public String toString() {
		return this.getState()+"";
	}

}
