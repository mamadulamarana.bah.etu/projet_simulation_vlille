package vlille.user;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;

import vlille.station.Station;
import vlille.station.util.EmptyStationException;
import vlille.station.util.StationFullException;
import vlille.station.util.UserCannotRentException;
import vlille.vehicle.Rentable;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.BikeIsNotAvailableException;

public class User {
	
	private String name;
	private int age;
	private boolean rentingSomething;
	private HashMap<Vehicle, Station<?> > rentedVehicles;
	
	/**
	 * constructor
	 * @param name
	 * @param age
	 */
	public User(String name, int age) {
		this.name = name;
		this.age = age;
		this.rentingSomething = false;
		this.rentedVehicles = new HashMap<>();
	}

	/**
	 * Get the name
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * get the age
	 * @return the age
	 */
	public int getAge() {
		return this.age;
	}
	
	/**
	 * Make the user rent something, now he rent something
	 * @param <T>
	 * @param station
	 * @return
	 * @throws EmptyStationException 
	 * @throws UserCannotRentException 
	 * @throws BikeIsNotAvailableException 
	 */
	public <T extends Vehicle> T rentSomething(Station<T> station) throws UserCannotRentException, EmptyStationException, BikeIsNotAvailableException {
		int index = (int) (Math.random()*(station.getOccupiedSlot()));
		T rentedGoods = station.rentTo(this, index);
		this.rentingSomething = true;
		this.storeRented(station, rentedGoods);
		return rentedGoods;
	}
	
	/**
	 * get the Object rentedVehicles
	 * @return the rentedVehicles object
	 */
	public Vehicle getRentedVehicle() {
		List<Vehicle> vehicles = new ArrayList<>(this.getRentedVehicles().keySet());
		return vehicles.get(0);
	}
	
	/**
	 * replace rentedVehicles object in station which have free slot
	 * @throws StationFullException 
	 */
	public void returnRentedVehicle(Station<Vehicle> st, Vehicle rented) throws StationFullException {
		this.rentedVehicles.remove(rented);
		st.add(rented);
	}
	
	/**
	 * the object come back to the station
	 * @param station
	 * @param rented
	 */
	public void storeRented(Station<?> station, Vehicle rented) {
		this.rentedVehicles.put(rented, station);
	}
	
	/**
	 * get the list of rentedVehicles object
	 * @return list of rentedVehicles object
	 */
	public HashMap<Vehicle, Station<?> > getRentedVehicles() {
		return this.rentedVehicles;
	}
	
	/**
	 * get the renter who rent this user
	 * @return
	 */
	public Station<?> getRenter() {
		return this.rentedVehicles.get(0);
	}
	
	/**
	 * get the number of object Rented
	 * @return the number of rentedVehicles object
	 */
	public int getNumberRentedObject() {
		return this.rentedVehicles.size();
	}

	/**
	 * Know if user rent something
	 * @return true if he rent, False otherwise 
	 */
	public boolean AreRentingSomething() {
		return this.rentingSomething;
	}
	
	/**
	 * 
	 */
	public boolean equals(Object o) {
		if(! (o instanceof User) ) {
			return false;
		}
		User other = (User)o;
		return this.name.equals(other.name) && this.age == other.age;
	}
	
	/**
	 * description of a user
	 */
	public String toString() {
		return ""+ this.getName()+", "+this.getAge()+" years Old";
	}

}
