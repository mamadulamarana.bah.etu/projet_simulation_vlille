package vlille.user;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.ClassicBike;
import vlille.vehicle.bike.ElectricBike;
import vlille.vehicle.util.Basket;
import vlille.vehicle.util.VehicleWithRack;
import vlille.vehicle.util.VehicleDecorator;

public class Mechanic extends AbstractVisitor{

	/**
	 * to visit a classic bike for repair
	 */
	public void visit(ElectricBike bike) {
		bike.control();
	}

	/**
	 * to visit an electric bike or repair
	 */
	public void visit(ClassicBike bike) {
		bike.control();
	}
	
	/**
	 * 
	 */
	public void visit(VehicleDecorator decoratedVehicle) {
		decoratedVehicle.control();
	}
	
	/**
	 * try to fix vehicle. if mechanic can have access to vehicle
	 * @param v vehicle we want to visit
	 */
	public void fixVehicle(Vehicle v) {
		v.accept(this);
	}	

}
