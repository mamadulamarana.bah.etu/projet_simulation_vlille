package vlille.user;

import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.*;
import vlille.vehicle.util.VehicleDecorator;

public interface Visitor {
	
	/**
	 * visit a vehicle to repair it or do something else
	 * @param v the specified vehicle
	 */
	public void visit(Vehicle v);
	
	/**
	 * visit an electric to repair it or do something else
	 * @param bike the specified vehicle
	 */
	public void visit(ElectricBike bike);
	
	/**
	 * visit an electric to repair it or do something else
	 * @param bike the specified vehicle
	 */
	public void visit(ClassicBike bike);
	
	/**
	 * 
	 * @param decoratedVehicle
	 */
	public void visit(VehicleDecorator decoratedVehicle);
}
