package vlille.user;

import vlille.vehicle.Vehicle;

public abstract class AbstractVisitor implements Visitor{
	
	/**
	 * 
	 */
	public AbstractVisitor() {
		
	};
	
	/**
	 * 
	 */
	public void visit(Vehicle vehicle) {
		vehicle.accept(this);
	}
}
