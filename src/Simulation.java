import java.util.ArrayList;

import java.util.List;
import java.util.Random;

import util.listchooser.ListChooser;
import util.listchooser.RandomListChooser;
import vlille.center.Center;
import vlille.station.BikeStation;
import vlille.station.Station;
import vlille.station.util.StationFullException;
import vlille.user.User;
import vlille.vehicle.Vehicle;
import vlille.vehicle.bike.ClassicBike;
import vlille.vehicle.bike.ElectricBike;
import vlille.vehicle.util.Basket;
import vlille.vehicle.util.VehicleWithRack;

public class Simulation {
	
	private static final int NB_USERS = 5;

	private int nbStations;
	private int nbVehicles;
	private List<Station<Vehicle>> stations;
	private List<Vehicle> vehicles;
	private List<User> users;
	private Center center;
	private int timeInterval;
	
	/**
	 * 
	 * @param intervalleTemps
	 */
	public Simulation(int nbStations, int nbVehicle, int timeInterval) {
		this.timeInterval = timeInterval;
		this.center = Center.getInstance();
		this.stations = new ArrayList<>();
		this.vehicles = new ArrayList<>();
		this.users = new ArrayList<>();
		this.init();
	}
	
	/**
	 * create stations for simulation
	 * @param nbStations number of station to create
	 */
	public void init() {
		this.initStations();
		this.initVehicles();
		this.fillStations();
		this.initUsers();
	}
	
	/**
	 * initialize users
	 */
	public void initUsers() {
		for (int i=0; i<Simulation.NB_USERS; i++) {
			this.users.add(new User("mamad "+i,17+i));
		}
	}
	
	/**
	 * 
	 */
	public void initStations() {
		for(int i=0; i<this.nbStations; i++ ) {
			Station<Vehicle> st = new BikeStation(i);
			st.setCenter(this.center);
			this.stations.add(st);
		}
	}
	
	/**
	 * 
	 */
	public void initVehicles() {
		int typeOfVehicle = new Random().nextInt(0, 1);
		Vehicle vehicle = null;
		for(int i=0; i<(this.nbVehicles/2); i++) {
			if(typeOfVehicle == 0) {
				vehicle = new ClassicBike(i);
				this.vehicles.add(vehicle);
			}
			else {
				vehicle = new ElectricBike(i);
			}
		}
		this.decorVehicle(vehicle);
	}
	
	/**
	 * 
	 * @param vehicle
	 */
	public void decorVehicle(Vehicle vehicle) {
		int typeOfDecoration = new Random().nextInt(0, 1);
		Vehicle v = null;
		for (int i=0; i<(this.nbVehicles/2); i++) {
			if(typeOfDecoration == 0) {
				v = new Basket(vehicle);
			}
			else {
				v = new VehicleWithRack(vehicle);
			}
		}
		this.vehicles.add(v);
	}
	
	/**
	 * fill all stations that have been created
	 */
	public void fillStations() {
		List<Vehicle> remainingVehicle = this.vehicles;
		int stationIndex = new Random().nextInt(0, this.nbStations);
		int vehicleIndex = new Random().nextInt(0, remainingVehicle.size());
		while ( (remainingVehicle.size()) > 0) {
			try {
				this.stations.get(stationIndex).add(this.vehicles.get(vehicleIndex));
				remainingVehicle.remove(vehicleIndex);
				vehicleIndex = new Random().nextInt(0, remainingVehicle.size());
			}catch(StationFullException e) {
				stationIndex = new Random().nextInt(0, this.nbStations);
			}
		}
	}
	
	public void simulation() {
		List<String> choice = new ArrayList<>();
		choice.add("oui"); choice.add("non");
		ListChooser<String> ilc = new RandomListChooser<>();
		String readChoice = ilc.choose("Voulez-vous lancer la simulation ", choice);
		if(readChoice.equals("oui")){
			
		}
		else {
			System.out.println("dommage...");
		}
	}

}
